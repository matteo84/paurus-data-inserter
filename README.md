# Data parsing and insertion

This is a spring boot application that has one REST entry point:
/inserter/start

By doing a GET request to that endpoint we start the processing of the fo_random.txt
 file located in the resources folder. 

The application will parse the file and insert the parsed data to a postgresql database.
  
# Starting the application and requirements

A PostgreSQL database is required to run the application.
The DB should be listening on port 5432 and a database named 'paurus_data_insert' should exist and be
accessible with user/password postgres/postgres.
 
**Running the application:**
* In IntelliJ IDEA you can run/debug the App class

* In a terminal using maven you can run the application by running the command: 
    mvn spring-boot:run
    
    
After the application has started do a GET request to http://localhost:8080/inserter/start

# Other info

The db creation strategy is set to create-drop, so when the spring boot app is shut down the db gets
destroyed. Each time then when we run the application we have a clean DB.

The parsing of the file and sorting takes 1s so there is no sense in parallel processing of the sorting. 
The bottleneck are the db insertions. 

**Min insert date: 2020-10-14 23:31:39**

**Max insert date: 2020-10-14 23:32:06**

**Insertion time=27s**

**Total running time = 28s**
