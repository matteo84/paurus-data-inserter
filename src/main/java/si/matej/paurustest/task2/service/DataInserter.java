package si.matej.paurustest.task2.service;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.matej.paurustest.task2.model.MatchData;

public class DataInserter extends Thread {

    private final Logger LOG = LoggerFactory.getLogger(DataInserter.class);

    private EntityManagerFactory entityManagerFactory;
    private final List<List<MatchData>> queue = new LinkedList<>();

    public DataInserter(final EntityManagerFactory entityManagerFactory) {
        super();
        this.entityManagerFactory = entityManagerFactory;
    }

    /***
     * Thread starts when a new element is added to the queue and runs until there are elements in the queue
     */
    public void run() {
        LOG.info("Starting thread " + currentThread().getName() + ", " + this);
        while (!queue.isEmpty()) {
            final List<MatchData> matchData = queue.get(0);
            queue.remove(0);
            batchInsertMatch(matchData);
        }
    }

    //Add element to queue and starts the thread if it is not running
    public void addToQueue(final List<MatchData> matchDataList) {
        queue.add(matchDataList);
        if (!isAlive()) {
            start();
        }
    }

    /**
     * Insert entities in bulks. The JpaRepository's saveAll performs slower than inserting with
     * entity manager and managing transactions manually.
     *
     * @param matchList - list of entities to be inserted
     */
    public void batchInsertMatch(final List<MatchData> matchList) {
        //matchDataRepository.saveAll(matchList);
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        final int batchSize = 100;

        try {
            entityTransaction.begin();

            int i = 1;
            for (final MatchData entity : matchList) {
                if (i % batchSize == 0) {

                    entityManager.flush();
                    entityManager.clear();

                    entityTransaction.commit();
                    entityTransaction.begin();
                }

                entityManager.persist(entity);
                i++;
            }

            entityTransaction.commit();
        } catch (final RuntimeException e) {
            if (entityTransaction.isActive()) {
                entityTransaction.rollback();
            }
        } finally {
            entityManager.close();
        }
    }
}
