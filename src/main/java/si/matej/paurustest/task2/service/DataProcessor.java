package si.matej.paurustest.task2.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import si.matej.paurustest.task2.model.MatchData;
import si.matej.paurustest.task2.repository.MatchDataRepository;

@Service
public class DataProcessor {

    final Comparator<MatchData> matchComparator = Comparator.comparing(MatchData::getMatchId)
            .thenComparing(MatchData::getOutcomeId)
            .thenComparing(MatchData::getSpecifiers);

    private EntityManagerFactory entityManagerFactory;

    @Autowired
    public DataProcessor(final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void process() throws IOException {

        final DataInserter dataInserter = new DataInserter(entityManagerFactory);

        final InputStream input = getClass().getClassLoader().getResourceAsStream("fo_random.txt");
        final BufferedReader reader = new BufferedReader(new InputStreamReader(input));

        final Map<Integer, List<MatchData>> matchesMap = new TreeMap<>();


        // parse the file and group all entries by matchid in a treemap
        // with this map we have the keySet sorted by the key - matchid
        String line;
        while ((line = reader.readLine()) != null) {

            line = line.replaceAll("'", "");

            if (line.startsWith("sr:match")) {
                final String[] split = line.split("\\|");

                final int matchId = Integer.parseInt(split[0].replaceAll("sr:match:", ""));
                final int marketId = Integer.parseInt(split[1]);
                final String outcomeId = split[2];

                String specifiers = "";
                if (split.length > 3) {
                    specifiers = split[3];
                }

                List<MatchData> matchDataList = null;
                if (matchesMap.containsKey(matchId)) {
                    matchDataList = matchesMap.get(matchId);
                } else {
                    matchDataList = new ArrayList<>();
                    matchesMap.put(matchId, matchDataList);
                }

                final MatchData matchData = new MatchData();
                matchData.setMatchId(matchId);
                matchData.setMarketId(marketId);
                matchData.setOutcomeId(outcomeId);
                matchData.setSpecifiers(specifiers);

                matchDataList.add(matchData);
            }
        }

        //set of ordered keys (match_ids)
        final Set<Integer> matches = matchesMap.keySet();

        matches.forEach(mid -> {
            final List<MatchData> matchDataList = matchesMap.get(mid);
            matchDataList.sort(matchComparator);
            dataInserter.addToQueue(matchDataList);
        });
    }
}