package si.matej.paurustest.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import si.matej.paurustest.task2.model.MatchData;

@Repository
public interface MatchDataRepository extends JpaRepository<MatchData, Long> {
}
