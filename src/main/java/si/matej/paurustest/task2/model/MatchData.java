package si.matej.paurustest.task2.model;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Data
public class MatchData {

    @Id
    private String id;

    @Column
    private int matchId;

    @Column
    private int marketId;

    @Column
    private String outcomeId;

    @Column
    private String specifiers;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInsert;

    @PrePersist
    protected void onCreate() {
        dateInsert = new Date();
    }

    public MatchData() {
        this.id = UUID.randomUUID().toString();
    }
}
