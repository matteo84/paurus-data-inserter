package si.matej.paurustest.task2.controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import si.matej.paurustest.task2.service.DataProcessor;

@RestController
@RequestMapping("/inserter")
public class DataInserterController {
    private final Logger LOG = LoggerFactory.getLogger(DataInserterController.class);

    private DataProcessor dataProcessor;

    @Autowired
    public DataInserterController(final DataProcessor dataProcessor) {
        this.dataProcessor = dataProcessor;
    }

    @GetMapping("/start")
    public void start() {
        try {
            dataProcessor.process();
        } catch (final IOException e) {
            LOG.error("Error occured while processing the fo_random file", e);
        }
    }
}

